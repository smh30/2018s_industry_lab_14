package ictgradschool.industry.swingworker.ex05;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.concurrent.ExecutionException;

/**
 * A shape which is capable of loading and rendering images from files / Uris / URLs / etc.
 */
public class ImageShape extends Shape {

    private Image image;
    private boolean loaded = false;


    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, String fileName) throws MalformedURLException {
        this(x, y, deltaX, deltaY, width, height, new File(fileName).toURI());
    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, URI uri) throws MalformedURLException {
        this(x, y, deltaX, deltaY, width, height, uri.toURL());
    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, URL url) {
        super(x, y, deltaX, deltaY, width, height);

        ImageLoadingWorker imgworker = new ImageLoadingWorker(url, width, height);
        imgworker.execute();


    }

    public class ImageLoadingWorker extends SwingWorker<Image, Void> {
        private URL url;
        // weird name for this image to prevent confusion over main image in done method
        private Image imaged;
        private int width;
        private int height;

        public ImageLoadingWorker(URL url, int width, int height) {
            this.url = url;
            this.width = width;
            this.height = height;
        }

        @Override
        protected Image doInBackground() {
            try {
                Image image = ImageIO.read(url);

                if (width == image.getWidth(null) && height == image.getHeight(null)) {
                    imaged = image;
                } else {
                    imaged = image.getScaledInstance(width, height, Image.SCALE_SMOOTH);
                }
                // calling getWidth so that the scaling will not lag the program
                imaged.getWidth(null);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return imaged;
        }

        @Override
        protected void done() {

            try {
                image = get();
                loaded = true;
            } catch (InterruptedException | ExecutionException e) {
                // do nothing
            }
        }
    }


    @Override
    public void paint(Painter painter) {

        // if the image is still loading, show a rectangle
        if (!loaded) {
            painter.setColor(Color.lightGray);
            painter.fillRect(fX, fY, fWidth, fHeight);
            painter.setColor(Color.red);
            painter.drawRect(fX, fY, fWidth, fHeight);
            painter.drawCenteredText("Loading....", fX, fY, fWidth, fHeight);
        }
//todo should be using propertyChange to tell when the things are loaded???????
        painter.drawImage(this.image, fX, fY, fWidth, fHeight);

    }
}
