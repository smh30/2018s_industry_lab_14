package ictgradschool.industry.swingworker.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Simple application to calculate the prime factors of a given number N.
 * <p>
 * The application allows the user to enter a value for N, and then calculates
 * and displays the prime factors. This is a very simple Swing application that
 * performs all processing on the Event Dispatch thread.
 */
public class IntPrimeFactorsSwingApp extends JPanel {

    private JButton _startBtn;        // Button to start the calculation process.
    private JTextArea _factorValues;  // Component to display the result.
    private JButton _abortBtn;
    private PrimeFactorisationWorker pfworker;

    public IntPrimeFactorsSwingApp() {
        // Create the GUI components.
        JLabel lblN = new JLabel("Value N:");
        final JTextField tfN = new JTextField(20);

        _startBtn = new JButton("Compute");
        _factorValues = new JTextArea();
        _factorValues.setEditable(false);
        _abortBtn = new JButton("Abort");
        _abortBtn.setEnabled(false);

        // Add an ActionListener to the start button. When clicked, the
        // button's handler extracts the value for N entered by the user from
        // the textfield and find N's prime factors.
        _startBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                String strN = tfN.getText().trim();
                long n = 0;

                try {
                    n = Long.parseLong(strN);
                } catch (NumberFormatException e) {
                    System.out.println(e);
                }

                // Disable the Start button until the result of the calculation is known.
                _startBtn.setEnabled(false);
                _abortBtn.setEnabled(true);
                // Clear any text (prime factors) from the results area.
                _factorValues.setText(null);
                // Set the cursor to busy. why isn't this working???
                setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));


                pfworker = new PrimeFactorisationWorker(n);
                pfworker.execute();


            }
        });

        //add actionlistener to the abort button which will cancel the computation
        _abortBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                System.out.println("clicked abort button");

                pfworker.cancel(false);
            }
        });

        // Construct the GUI.
        JPanel controlPanel = new JPanel();
        controlPanel.add(lblN);
        controlPanel.add(tfN);
        controlPanel.add(_startBtn);
        controlPanel.add(_abortBtn);

        JScrollPane scrollPaneForOutput = new JScrollPane();
        scrollPaneForOutput.setViewportView(_factorValues);

        setLayout(new BorderLayout());
        add(controlPanel, BorderLayout.NORTH);
        add(scrollPaneForOutput, BorderLayout.CENTER);
        setPreferredSize(new Dimension(500, 300));
    }

    private static void createAndShowGUI() {
        // Create and set up the window.
        JFrame frame = new JFrame("Prime Factorisation of N");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Create and set up the content pane.
        JComponent newContentPane = new IntPrimeFactorsSwingApp();
        frame.add(newContentPane);

        // Display the window.
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        // Schedule a job for the event-dispatching thread:
        // creating and showing this application's GUI.
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }

    public class PrimeFactorisationWorker extends SwingWorker<List<Long>, Long> {
        private long n;

        public PrimeFactorisationWorker(long n) {
            this.n = n;
        }


        @Override
        protected List<Long> doInBackground() throws InterruptedException {
            //create the list which will be returned by this method
            List<Long> results = new ArrayList<Long>();


            // Start the computation in the background
            for (long i = 2; i * i <= n; i++) {

                // If i is a factor of N, repeatedly divide it out
                while (n % i == 0) {
                    results.add(i);
                    publish(i);
                    n = n / i;
                    Thread.sleep(1000);
                }
            }

            // if biggest factor occurs only once, n > 1
            if (n > 1) {
                results.add(n);
                publish(n);
            }

            return results;
        }

        @Override
        protected void process(List<Long> chunks) {
            if(!isCancelled()){
            for (Long chunk : chunks) {
                _factorValues.append(chunk.toString() + "\n");
            }

     }

        }

        @Override
        protected void done() {

                _startBtn.setEnabled(true);
                _abortBtn.setEnabled(false);
                setCursor(Cursor.getDefaultCursor());


        }
    }
}

